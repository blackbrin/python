
"""
lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]

将列表lis中的”k”变成大写，并打印列表。
将列表中的数字3变成字符串”100”
将列表中的字符串”tt”变成数字 101
在 “qwe”前面插入字符串：”火车头”

"""

list_a = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]

# 将列表lis中的”k”变成大写，并打印列表。
list_a[2] = "K"
print(list_a)

# 将列表中的数字3变成字符串”100”
list_a[1] = "100"
list_a[3][2][1][1] = "100"
print(list_a)  # 将列表中的数字3变成字符串”100”


# 将列表中的字符串”tt”变成数字 101
list_a[3][2][1][0] = "101"
print(list_a)  # 将列表中的字符串”tt”变成数字 101


# 在 “qwe”前面插入字符串：”火车头”
list_a[3].insert(0,"火车头")
print(list_a)



print("-"*30)

"""
写代码，有如下列表，按照要求实现每一个功能。


"""


list_b = ["alex", "WuSir", "ritian", "barry", "wenzhou"]

# 计算列表的长度并输出
list_b_size = len(list_b)
print(list_b_size)


# 请获取索引为偶数的所有值，并打印出获取后的列表
list_b_2 = list_b[::2]
print(list_b_2)

# 列表中追加元素”seven”,并输出添加后的列表
list_b.append("seven")
print(list_b)

# 请在列表的第1个位置插入元素”Tony”,并输出添加后的列表
list_b.insert(0,"Tony")
print(list_b)

# 请修改列表第2个位置的元素为”Kelly”,并输出修改后的列表
list_b[1] = "Kelly"
print(list_b)

# 请将列表的第3个位置的值改成 “太白”，并输出修改后的列表
list_b[2] = "“太白”"

# 请将列表 l2=[1,”a”,3,4,”heart”] 的每一个元素追加到列表li中，并输出添加后的列表

l2 = [1, "a",3,4,"heart"]
print(list_b + l2)

# 请将字符串 s = “qwert”的每一个元素添加到列表li中，一行代码实现，不允许循环添加。

list_b.extend(list("qwert"))
print(list_b)


# 请删除列表中的元素”ritian”,并输出删除元素后的列表
list_b.remove("ritian")
print(list_b)

# 请删除列表中的第2个元素，并输出删除元素后的列表

list_b.pop(1)
print(list_b)

# 请删除列表中的第2至第4个元素，并输出删除元素后的列表

del list_b[1:3]
print(list_b)




dic_a = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}


# 请在字典中添加一个键值对，"k4": "v4"，输出添加后的字典
dic_a["k4"] = "v4"

print(dic_a)


# 请在修改字典中 "k1" 对应的值为 "alex"，输出修改后的字典
dic_a["k1"] = "alex"
print(dic_a)

# 请在k3对应的值中追加一个元素 44，输出修改后的字典

dic_a["k3"].append("44")
print(dic_a)


#请在k3对应的值的第 1 个位置插入个元素 18，输出修改后的字典
dic_a["k3"].insert(0,18)
print(dic_a)




dic_b = {
'name':['alex',2,3,5],
'job':'teacher',
'oldboy':{'alex':['python1','python2',100]}
}

# 1，将name对应的列表追加一个元素’wusir’。

dic_b["name"].append("wusir")
print(dic_b)


# 2，将name对应的列表中的alex首字母大写。
print(dic_b["name"][0].capitalize())


# 3，oldboy对应的字典加一个键值对’老男孩’,’linux’。

dic_b["oldboy"]["老男孩"] = "linux"

print(dic_b)


# 4，将oldboy对应的字典中的alex对应的列表中的python2删除
dic_b["oldboy"]["alex"].remove("python2")
print(dic_b)


"""











"""




