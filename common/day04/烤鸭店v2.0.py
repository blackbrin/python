""""""
"""
加入其他的菜品功能 -- 录入功能
菜名 ， 成本价， 产地， 生产日期
烤鸭  10  xx农场 2021-10-14 
大葱   1  xx基地  2021-10-13
"""

#  烤鸭店系统 需求:
# 1. 菜品信息 录入功能
# 2. 搜索功能  输入烤鸭  检索出 烤鸭详细信息
# 3. 退出功能


while True:
    oper_type = input("1-录入菜品\n2-查询菜单\n3-退出系统\n请做出您的选择(输入序号:)")
    if oper_type == "1":
        print(">>>>>进入录入系统，请按照要求输入:")

    elif oper_type == "2":
        print(">>>>>>进入查询系统")
    else:
        print("退出系统")
        break



import copy
# 定义一个空列表
listA = []

# 定义一个非空列表
listB = ["felix","apple"]
listC = ["felix",100,12.4,listB,True,False,[1,3,4,5]]

# 翻转
list_D = [1,3,4,5,6,7]
list_E = copy.copy(list_D)
list_E.reverse()
print(list_D)
print(list_E)


# 列表取值---单个取值，索引
result_A = listC[3]
result_B = listC[5]
result_C = listC[0]
result_D = listC[-1]
result_E = listC[-3]
result_F = listC[-5]


# 列表取值 --- 多个取值[start:end:step]
result = listC[:]
resultA = listC[::]
resultB = listC[0:]
resultC = listC[1:5]
resultD = listC[0:len(listC):2]
resultE = listC[::-1]
resultF = listC[-2:1:-1]
resultG = listC[-2:-6:-1]
resultH = listC[:3:-1]
resultI = listC[:3:1]



# 定义一个非空列表
listA = ["felix","apple"]
listB = ["felix",100,12.4,True,False,[1,3,4,5]]

# 增加元素---增加一个元素
listA.append("blackbrin")

# 增加元素---增加一个列表,本质还是增加一个元素
listB.append(listA)
print(listB)


# 增加元素---insert,指定位置插入一个元素
listA.insert(0,"blackbrin")
listA.insert(1,"carrot")

# 增加元素---insert,指定位置插入一个列表
listB.insert(2,listA)

listB.insert(-1,listA)



# 增加元素---extend，单个 字符串
listA.extend("felix")

#增加元素---extend，单个 数字，TypeError: 'int' object is not iterable
#listA.extend(100)

# 增加元素---extend，列表
listB.extend(listA)


# 删除---指定存在的元素
listA.remove("felix")

# 删除-指定不存在的元素，报错，ValueError: list.remove(x): x not in list
# listA.remove("coot")

# 删除重复的元素，仅删除找到的第一个元素
listB.remove("red")



#删除 --pop(index),添加索引
listB.pop(2)

# 删除--pop(),不添加索引
listB.pop()

# 删除--pop(index),索引不存在,IndexError: pop index out of range
#listA.pop(2)




# 清空列表
#listA.clear()


# 删除 del 列表名[索引值]，单个

del listA[1]

# 删除 del 列表名[索引值]，--del 列表名[start:end:step]多个

del listB[2:7:2]



# 改，单个值

listA[2] = "banana"

# 改，多个值
listC = listB[:]

listC[2:5] = "carrot"

listD = listB[:]

listD[2:5] = ["felix"]




# 反转，reverse()
listC = listB[:]
listC.reverse()


# 获取索引值
print(listB.index("red"))
print(listB.index(12.4))


# 统计元素个数
print(listB.count("felix"))

listC = listB[:]
listC.extend(listA)
print(listC.count("felix"))




# 定义一个空元祖
tupleA= ()

# 定义一个非空元祖，仅包含一个元素,元素的后面必须要有一个逗号
tupleB = ("felix",)


# 定义一个非空元祖，包含多个元素，使用逗号隔开
tupleC = ("felix",100,12.4,"red",True,False,[1,3,4,5],"red")


# 查看元祖的类型
print(type(tupleC))

# 获取元祖的长度
print(len(tupleC))

# 元祖的拼接

tupleD = tupleC + tupleB

# 元祖的多次输出
tupleE = tupleB * 3

