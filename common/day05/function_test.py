import sys

# 获取最大值
def max(num1,num2):
    if num1 > num2:
        return num1
    elif num1 < num2:
        return num2
    else :
        return "{}和{}一样".format(num1,num2)



# 获取列表中奇数索引
def get_list_odd(list_data):
    list_new = []
    for i in range(len(list_data)):
        if i % 2 !=0:
            list_new.append(list_data[i])

    return list_new

# 3.写函数，判断用户传入的对象（列表）长度是否大于5，如果大于5，那么仅保留前五个长度的内容并返回。不大于5返回本身
def get_list_len(list_data):
    list_len = len(list_data)

    if list_len > 5:
        return list_data[0:5]
    else:
        return list_data


# 4.写函数，检查传入的字符串是否含有空格，并返回结果，包含空格返回True，不包含返回False
def str_isContain_sapce(str):
    if str.count(" "):
        return True
    else:
        return False



# 5.写函数，接收n个数字，分别写4个函数求n数字的和、差、商、积
def calc(list_num,method):
    list_num_new = [str(x) for x in list_num]
    if method == "+":
        sum = 0
        for i in list_num:
            sum += i

        return  method.join(list_num_new) + "={}".format(sum)

    elif method == "-":
        difference = list_num[0]
        for i in list_num[1:]:
            difference -= i
        return method.join(list_num_new) + "={}".format(difference)
    elif method == "*":
        accumulate = 1
        for i in list_num:
            accumulate *= i

        return method.join(list_num_new) + "={}".format(accumulate)

    elif method == "/":
        quotient = list_num[0]
        for i in list_num[1:]:
            quotient /= i
        return method.join(list_num_new) + "={}".format(quotient)
    else:
        return "{}输入不正确".format(method)



# 5.写函数，接收n个数字，分别写4个函数求n数字的和、差、商、积
def calc_extend(method,*args):
    list_num_new = [str(x) for x in args]
    if method == "+":
        sum = 0
        for i in args:
            sum += i

        return  method.join(list_num_new) + "={}".format(sum)

    elif method == "-":
        difference = args[0]
        for i in args[1:]:
            difference -= i
        return method.join(list_num_new) + "={}".format(difference)
    elif method == "*":
        accumulate = 1
        for i in args:
            accumulate *= i

        return method.join(list_num_new) + "={}".format(accumulate)

    elif method == "/":
        quotient = args[0]
        for i in args[1:]:
            quotient /= i
        return method.join(list_num_new) + "={}".format(quotient)
    else:
        return "{}输入不正确".format(method)


def str_args(*args):
    print(args)  #  ([1, 2, 3, 4, 5, 6], 1, 3, 4, 5, 6)  元祖
    print(*args) # [1, 2, 3, 4, 5, 6] 1 3 4 5 6  原样输出



def opear(option,stuInfolist):
    if option == "0":
        getAllStuInfo(stuInfolist)
    elif option == "1":
        add(stuInfolist)
    elif option == "2":
        delStuInfo(stuInfolist)
    elif option == "3":
        updateStuinfo(stuInfolist)
    elif option == "4":
        searchStuInfo(stuInfolist)
    else:
        print("退出系统")
        sys.exit()


def add(stuInfolist):
    name = input("请输入学生姓名:")
    stuInfolist.append(name)
    if name in stuInfolist:
        print("{}学生添加成功".format(name))

def getAllStuInfo(stuInfolist):
    print("所有的学生信息:",stuInfolist)


def delStuInfo(stuInfolist):
    name = input("请输入要删除学生的名字:")
    if name in stuInfolist:
        stuInfolist.remove(name)
        print("{}删除成功".format(name))
    else:
        print("{}不在学生档案中，请先查询".format(name))


def updateStuinfo(stuInfolist):
    old_name = input("请输入更新学生的姓名:")
    if old_name not in stuInfolist:
        print("{}学生的姓名不存在".format(old_name))
    else:
        new_name = input("请输入更改后的学生姓名:")
        print(stuInfolist.index(old_name))
        stuInfolist[stuInfolist.index(old_name)] = new_name
        print("{}修改成{}".format(old_name,new_name))




def searchStuInfo(stuInfolist):
    name = input("请输入查询的学生姓名:")
    if name in stuInfolist:
        print("{}学生信息查询成功！".format(name))
    else:
        print("{}学生不在档案中".format(name))



def run():
    AllstuInfo = []
    while True:
        str_desc = """
        -----------------------欢迎进入V213班学生管理系统-----------------------------
                                0:显示所有学员信息
                                1:添加一个学员信息
                                2:删除一个学员信息
                                3:修改一个学员信息
                                4:查询一个学员信息
                                q:exit:退出学生管理系统
                                请选择系统功能：
        """

        option = input(str_desc)
        opear(option,AllstuInfo)




if __name__ == '__main__':
    run()

    # print(max(10,20))
    # list = [34, 23, 52, 352, 352, 3523, 5]
    # list_2 = [34,23,52,352,666,3523,5]
    # list_3 = [34,23,52,4,5,9]

    # resut = get_list_odd(list)
    # print(resut)

    # result = get_list_len(list_3)
    # print(result)
    #
    # str_1 = "Hello  World"
    # print(str_isContain_sapce(str_1))
    # list_4 = [1,2,3,4,5,6]
    # print(calc(list_4,"/"))
    # print(calc_extend("/",1,2,3,4,5,6))
    #
    # str_args(list_4,1,3,4,5,6)

    """
    有字符串s = "123a4b5c"
    通过对s切片形成新的字符串 "a4b"
    通过对s切片形成新的字符串 "2abc"
    """
    # s = "123a4b5c"
    # print(s[3:6])
    # print(s[1::2])

    """
    li = [1, 3, 2, "a", 4, "b", 5,"c"]

    通过对li列表的切片形成新的列表 [1,3,2]
    通过对li列表的切片形成新的列表 [“a”,4,”b”]
    通过对li列表的切片形成新的列表 [1,2,4,5]
    通过对li列表的切片形成新的列表 [3,”a”,”b”]
    通过对li列表的切片形成新的列表 [3,”a”,”b”,”c”]
    通过对li列表的切片形成新的列表 [“c”]
    通过对li列表的切片形成新的列表 [“b”,”a”,3]
    """
    # li = [1, 3, 2, "a", 4, "b", 5, "c"]
    # print(li[0:3])
    # print(li[3:6])
    # print(li[0::2])
    # print(li[1:6:2])
    # print(li[1::2])
    # print(li[-1:])
    # print(li[-3:-8:-2])
