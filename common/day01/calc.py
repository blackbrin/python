import  sys
print("欢迎使用简单计算机,本计算器仅支持+,-,*,/")
print("输入q则退出计算器。")

while True:
    num_1 = int(input("请输入第一个数值:"))
    calc_type = input('请输入计算方式“+”、"-"、"*"、"/"，输入q则退出:')
    num_2 = int(input("请输入第二个数值:"))

    result = None

    if "+" == calc_type:
        result = num_1 + num_2
    if "-" == calc_type:
        result = num_1 - num_2
    if "*" == calc_type:
        result = num_1 * num_2
    if "/" == calc_type:
        if num_2 == 0:
            print("除法运算,第二个数值不能为0，重新开始:")
            continue
        result = num_1 / num_2
    if "q" == calc_type:
        print("退出计算")
        sys.exit()

    print("计算的结果是{}{}{}={}".format(num_1,calc_type,num_2,result))



