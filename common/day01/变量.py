# 单个变量赋值,2个位置存储
age1 = 1000
age2 = 1000

print(age1,id(age1))
print(age2,id(age2))

# 连续赋值,一个位置内存
age1 = age2 = 2000

print(age1,age2)


# 多个变量,赋不同的值
name,age,sex = "felix",18,"男"
print(name,age,sex)

# * 号的处理
# * 号，将多个值给到一个变量

a,b,*c = 1,2,3,4,5,6,7
print(a)
print(b)
print(c)



