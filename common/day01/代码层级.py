"""
什么是代码层级

判断、循环、函数、类等都是层级关系

python如何控制层级？

用缩进来表示层级，同一个级别的缩进一定要保持一致，Python推荐使用tab来进行缩进，不推荐使用空格


"""


class Person(object):
    def __init__(self,name,age):
        self.name = name
        self.age = age

    def run(self,distance):
        print("run"+distance)

    def eat(self):
        if self.age >18:
            print("可以吃糖")
        else:
            print("不可以吃糖")