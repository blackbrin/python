print(7%2)
print(7//2)

num1 = 10
num2 = 20

rst = num1 > num2 and num1 + 10 == num2 or (not num2)

print(rst)

data = 3

bol = True

f1 = 2.12

print(data + f1)
print(bol + f1)

print("felix" + "res")
print("felix" * 10)

print("h" in "felixh")
print("felx" not in "felix")