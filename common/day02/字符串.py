# 定义一个空字符串
str1 = ''
str2 = ""
str3 = ''' '''
str4 = """ """

print(str1,str2,str3,str4)
print(len(str1),len(str2),len(str3),len(str4))

# 定义一个非空字符串
str5 = 'python'
str6 = "java"
str7 = '''php c++'''
str8 = """c nihao """
print(len(str5))

# 字符串拼接
print(str8 + str7)

# 多次输出
print(str5 * 5)


# 特殊字符处理

print("你好\n 的 是的")
print(r"你好\n 的 是的")
print(R"你好\n 的 是的")