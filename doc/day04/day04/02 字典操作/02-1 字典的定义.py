""""""
"""字典:
字典:具有键值映射关系的一组无序数据组合.  key:value
标识符 {}
关键字:dict
无序 没有索引值
字典中的元素是key:value的形式存储数据  键值对
    key：是唯一的不能修改的数据，支持数据类型有：int，float,bool,str,元组
    value:可以数据，支持任何数据类型
"""

# 学生 姓名zs 学号101 班级201
dic = {
    "name":"zs",
    "sno":101,
    "cls":"208"
}
print(dic["sno"])
print(type(dic))