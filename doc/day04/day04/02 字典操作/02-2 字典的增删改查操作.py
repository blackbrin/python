""""""
"""字典增删改查
增加 : 字典名[不存在的key] = 新值
修改： 字典名[存在的key] = 新值
删除： 
   pop(key)通过key进行删除 ,删除键值对,pop必须给到对应的key,不能为空
   popitem() 随机删除
   clear()方法,清空
查询 ： 字典名[key] -- key不存在会报错
"""

dic = {
    "sno":101,
    "name":"zs",
    "score":[100,120,150]
}

# 操作
print(dic)
dic["cls"] = 213
print(dic)

dic["cls"] = 209
print(dic)
print(dic["score"][1])

# 删除
# dic.popitem()
# print(dic)
dic.pop("name")
print(dic)
dic.clear()
print(dic)


