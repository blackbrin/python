""""""
"""列表
标识符:[] 
关键字: list
"""
list_1 = []
list_2 = [1, 2, 3, 4]
print(type(list_1))
print(type(list_2))
print(len(list_2)) # len

list_2.reverse()
print(list_2)

list_3 = [100, 3.14, True, "Huace", [1, 2, 3, 4]]