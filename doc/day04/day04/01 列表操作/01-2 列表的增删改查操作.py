""""""

""" 列表: 有序 可变
 有序 -- 每个元素 都有索引 从0开始 
 列表的取值 ： 变量名[索引值]
 列表切片: 变量名[start:end:step] 取头不取尾
 
 可变: 列表中元素可以进行增删改查
 列表是一种数据结构，它有对应的方法（函数）操作对应的列表
 用变量名. 调用这个列表的方法
"""

lst3 = [100, 3.14, True, "Huace", [1, 2, 3, 4]]
# print(len(lst3))
# #获取 3.14
# print(lst3[1])
# lst = lst3[4]
# print(lst3[4][2])
#
# """增加元素
# append() 向列表中添加元素 ， 会追加在原列表的最后面 一次，只能加一个
# """
# lis1 = [1,2]
# # lis1.append("python")
# print(lis1)
#
# """
# insert() 一次只能加一个元素，可以加在任意的指定位置
# 传递参数，两个 索引值，要插入的值
# """
# lis1.insert(0,"huace")
# print(lis1)
#
# """
# 列表和列表可以直接相加 迭代，把元素一个个放入到新列表中
# extend（） 效果一样
# """
# # lst3.extend(lis1)  # 不建议这样写 【可读性差】
# lst3.extend("xiaoming")
# lst3.extend([100]) # 数字是一个整体
# print(lst3)

"""删除元素
remove(值)
"""

lst3 = [100, 3.14, True, "Huace", [1, 2, 3, 4]]
lst3.remove("Huace") # 字符串大小写敏感
print(lst3)
"""
通过索引删除
"""
lst3.pop()# 不带索引，默认删除最后一行
print(lst3)

"""
修改 变量名[索引值] = 新值
"""
lst3[0] = 200
print(lst3)