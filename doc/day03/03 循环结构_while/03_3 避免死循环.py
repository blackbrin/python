""""""
''''''
'''while循环-避免死循环2

while后面的条件为恒定值
    引入continue和break
    添加内部判断条件
continue 和 break 区别： continue 中断一次循环。 break 结束/跳出循环
'''
'''break关键字:在循环中碰到break,就会跳出整个循环'''

while 2 > 1 :
    print("循环开始")
    continue
    print("python基础")
print("循环结束")