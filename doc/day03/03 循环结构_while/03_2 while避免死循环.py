""""""
''''''
'''while循环-避免死循环1

while后面的条件不为恒定值 【每次循环都会执行 判断条件】
    引入变量，并在内部实现变量的递增或递减
    变量与while后面的条件组合
'''

num = 0
while num < 3:
    print("大家好，欢迎来到循环世界")
    num += 1

print("循环结束了")