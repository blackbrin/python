# 逻辑 -->各种分支结构
# 布尔值
a = True
b = False

# 与 或 非
print(a and b) # 每一个都为True 最终结果才能为True
print(a or b) # 只要里面有一个为true 最终结果就是True
print(not b) # 取反 -杠精

# 比较优先级:
# not > and >or
# 比较 运算符 > 逻辑优先符
print(1 > 2 and 4 > 3)
print(1 > 2 or 4 > 3)
print(not 1<2)

print(True and False or True and (not False))
# True and False or True and True
# False or True and True
# False or True
# True

# 算数 比较 逻辑 -- 计算 》 比较 》逻辑 (not > and > or)
print(1+1>1+2 and 3+2<3-1 or True and (not True))
#  2 >3 and 5 < 2 or True and (not False)
# False and False or True and True
# True