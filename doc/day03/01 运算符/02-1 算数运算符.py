# # 加减乘除 + - * /
# num1 = 10
# num2 = 5
# num3 = 4
#
# print(num1 + num2) # 加法运算 15
# print(num1 - num2) # 减法运算 5
# print(num1 * num2) # 乘法运算 50
# print(num1 / num2) # 除法运算 2 保留小数
#
# ### 复杂用法 求余数
# print(num1 % num2)
#
# # 整除 --只会取结果部分整数
# print(num1 // num2)
#
# # 求幂
# print(num1 ** num2)

"""加法和乘法 特殊使用
加法： 针对字符串str 进行拼接
乘法： 多次输出字符串
"""
# str1 = "hello"
# str2 = "world"
# data = 3
# print(str1+str2)
# # print(str1+data)
# print(str1 * 3) # 多次输出一段字符串 hellohellohello

"""整数，浮点数，布尔值 ==>可以进行数学计算 number"""
data = 3
bo1 = True # True==1 False ==0
f2 = 2.122

print(data + f2) # 能不能相加？能 5.122
print(data + bo1) # 能不能相加? 能 True==1 False ==0
print(bo1+f2)

