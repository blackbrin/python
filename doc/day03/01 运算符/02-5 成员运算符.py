"""
判断指定内容是否在这个变量中 -- True False
in : 存在
 a in  b 存在 --True

 not in : 不存在

"""
str = "xia.oming"
print("." not in str)

"""不能用于数值，整数，浮点数，复数，布尔值"""
bo1 = "True"
print("r" in bo1)
# data  =100
# print(1 in data)