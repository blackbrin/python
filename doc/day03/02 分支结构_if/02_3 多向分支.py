""""
多项分支 方向 东南西北
if 条件1
  代码1
elif 条件2
    代码2
elif 条件3
    代码3
...
else:
  语句块
"""

# 买票 年龄60岁以上免票  12 -59 成人票
# 4-11 儿童票  0-3 岁 不买票
age = int(input("请输入年龄:"))
if age >=60 or 0< age <=3 :
    print("不需要买票")
elif age >=12 :
    print("买成人票")
elif age >=4:
    print("儿童票")