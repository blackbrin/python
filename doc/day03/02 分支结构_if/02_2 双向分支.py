"""
你不是男孩子，就是女孩子
语法 ：
if 条件:
  代码块
else:
  代码块
"""
name = input("用户名:")
if name == "admin":
    print("欢迎管理员")
    print("管理员请喝茶")
else:
    print("游客登录")
print("我是后续执行的代码")