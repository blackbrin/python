"""
理论上来说 if判断后面的条件可以是任意的东西，只需要运算结果是个布尔值就行
比较运算符 > < ==
逻辑运算符: and or not
成员运算符: in not in
也可以直接用bool值
   True 非0 非空   if a
   Fasle 0 空
"""

if 1000 :
    print("111")
else:
    print(2222)

if "A" in "xiaoming":
    print("x在字符中")
else:
    print("x不在字符中")