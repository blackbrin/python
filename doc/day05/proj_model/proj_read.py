# 读 用一个方法去读
def read(file_path):
    print("进入查询系统,数据载入数据....")  # 要查询，一定得有数据,加入模糊搜索?
    file = open(file_path, encoding="utf-8")
    content = file.readlines()
    # 希望展示的数据格式-- 一个字典表示一个商品信息.商品信息应当由字典展示
    rp_lst = []
    # 将列表数据组装成字典
    for line in content:  # 烤鸭,10,xx基地 ,xx\n  # 北京烤鸭,10,xx基地 ,xx\n
        # 字符串分割 split(分割的标志)
        if line == "" or line == "\n":
            continue  # 结束当前循环 开始下一次
        prs_lst = line.split(",")  # 返回一个列表
        ps_dic = {}
        ps_dic["商品名称"] = prs_lst[0]
        ps_dic["成本价"] = prs_lst[1]
        ps_dic["产地"] = prs_lst[2]
        ps_dic["生产日期"] = prs_lst[3].strip("\n")  # 去除某些指定的字符 如果不指定，默认去除两边的空格
        rp_lst.append(ps_dic)
    print("所有数据", rp_lst)