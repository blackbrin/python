def write(file_path,proje_list):
    print("菜单正在保存，请稍后...")
    write_file = open(file_path, encoding="utf-8", mode="w")
    wp_list = []  # writeLines 一次性把一个列表写到文件中
    for dc in proje_list:
        str = "{},{},{},{}".format(dc["菜品名称"],dc["成本价"]
                                   ,dc["产地"],dc["生产日期"])
         # 写文件不要循环写，效率太低,一次性写完
        wp_list.append(str)
    write_file.writelines(wp_list)
    write_file.close()
    print("菜单保存完成")
