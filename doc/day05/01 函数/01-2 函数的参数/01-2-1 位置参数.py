""""""
"""
def 函数名(参数1..)

"""

"""1位置参数"""

def myself(name,adr,cls="V213",job="测试工程师"):
    print("""
    ===自我介绍== 
    姓名:{}
    住址:{}
    职业:{}
    班级:{}
    """.format(name,job,adr,cls))

# myself("深圳","小明","福田区")

"""指定参数"""
myself("小明","总统","福田区")

"""
不定长参数
参数的个数不知道 
*arg 可以接受多个参数，多个参数存在一个元组里
**kwargs 可以接受多个参数 多个参数存在一个字典里

"""

*a,b,c = 1,2,3,4,5,6,7,8
print(a,b,c)

# def add(*args):
#     rs = 0
#     print(args)
#     for i in args:
#         rs += i
#     return rs
#
# s = add(10,20,30)
# print(s)


def add(**kwargs):
    print(kwargs)

s = add(a=10,b=20,c=30)
