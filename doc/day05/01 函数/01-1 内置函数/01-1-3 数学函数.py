""""""
"""
数学计算中会用到函数，这些函数只要用就行了
abs() 求绝对值
divmod() 返还商和余数
round() 四舍五入
pow（） 幂次方
sum（） 求和
min（）最小值
max() 最大值
"""

# res = abs(-1)
# print(res)

# x = int(input("输入一个数:"))
# a,b = divmod(x,2)
# if b ==0 :
#     print("除数不能为0")
# print(a)
# print(b)

# print(round(2.5)) 所有的计算机语言都是这样

# print(pow(10,2)) == 10 ** 2


lst = [1,20,30,100]
print(sum(lst))
print(max(lst))
print(min(lst))

print(sum((1,20,39,100)))  # sum 只可以接受能迭代的数据 元组

print(min(1,20,30,199)) #
