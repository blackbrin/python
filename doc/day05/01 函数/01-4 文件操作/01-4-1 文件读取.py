""""""
"""python能够直接读取计算机文件
file文件使用 
1: 打开文件 open("文件路径",mode(方式))
     mode: r 只读  w 覆盖写  a 追加写
           rb      wb        ab    二进制
           r+      w+        a+    读写
2.文件操作: 读 写入
3.关闭文件（写操作）
"""
file = open(r"D:\temp\caidan.txt",encoding="utf-8")
# msg = file.read()
# print(msg) # 编码概念: -- UTF-8 GBK(支持中文) -IOS-8859(支持英文)
# part_msg = file.read(5) # 指定长度
# print(part_msg)
# file.close()

line1 = file.readline() # 读一行内容 默认情况
print(line1)
line2 = file.readline()
print("这是第二次的内容:",line2)
file.close()

# content = file.readlines()
# print(content,type(content))
# for i in content:
#     print(i)