
''''''
'''私有属性封装 
装饰器

'''
class Accoount():
    def __init__(self,name,money):
        self.__name=name
        self.__money=money

    @property    #get_name--返回出去
    def name(self):
        return self.__name

    @name.setter  #set_name--更改属性值
    def name(self,n):
        self.__name = n
        return self.__name

    @property  #装饰器，直接当做属性来调用  固定的
    def money(self):
        return self.__money

    @money.setter
    def money(self,m):  #m 你取钱的值
        if (isinstance(m,int) or  isinstance(m,float)):
            if  0< m <= self.__money:
                self.__money -= m
            else:
                print('余额不足')
        else:
            print('取款值不合法')
        print(self.__money)

zs=Accoount('zs',1000)
print(zs.money)  #def money(self)
zs.money=100

