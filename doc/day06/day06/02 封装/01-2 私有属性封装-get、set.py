
''''''
'''私有属性封装

私有属性在类外面无法访问，虽然保证了数据的安全性，但是也带来了不便。
属性的封装，可以在保证数据的安全性的同时也提供访问的接口。
'''

# 设计一个银行类，类中有姓名和金额，其中有几个需要注意的问题，
# 首先要隐藏细节，不能让用户随便修改金额；
# 如果确实要修改金额，则要提供访问访问接口（set/get方法），
# 然后还要避免在操作类的 对象过程中的误操作，比如修改金额传入非法数据，
# 为了保证数据的有效性，要对传入的数据进行判断。

# 类名：Accoount
# 属性:姓名和金额    构造函数
# 方法：取钱  查看余额
class Accoount():
    def __init__(self,name,money):
        self.__name=name
        self.__money=money

    # 提供一个接口供类外面的获取self.__money的值
    def get_money(self):
        return self.__money

    # 提供一个接口供类外面来设置self.__money的值
    def set_money(self,m):  #m 你取钱的值
        if (isinstance(m,int) or  isinstance(m,float)):
            if  0< m <= self.__money:
                self.__money -= m
            else:
                return '余额不足'
        else:
            return '取款值不合法'
        return  self.__money


zs=Accoount('zs',1000)
# print(zs.__money) 私有属性 不能直接用
print(zs.get_money())
print(zs.set_money(100))
print(zs.get_money())
# print(zs.set_money(100.560))
# print(zs.set_money('100'))




















# class Account():
#     def __init__(self,name,money):
#         self.__name=name # 账户姓名
#         self.__money=money  # 账户余额
#     # 账户性，创建账户时就确定，无允许修改，所以不提供姓名修改的set
#     def get_name(self):
#         return self.__name
#     # 获取账户余额
#     def get_money(self):
#         return self.__money
#     # 改变账户余额，对取款的值进行判断，并返回剩余的余额
#     def set_money(self,m):
#         if isinstance(m,int) or isinstance(m,float):
#             if m<=self.__money:
#                 self.__money -= m
#             else:
#                 print('额度不够')
#         else:
#             print('输入金额不正确')
#         return self.__money
#
# zs=Account('zs',1000)
# print(zs.get_name())
# print(zs.get_money())
# print(zs.set_money(500))
# print(zs.set_money(100.5))


