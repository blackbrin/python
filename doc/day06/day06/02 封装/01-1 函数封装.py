
''''''
'''函数封装
不同对象公有的方法可以通过函数封装，从而达到复用代码的作用
'''

# 需求：
# 小明体重75.0公斤
# 每次跑步会减肥0.5公斤
# 每次吃东西体重会增加1公斤
# 小美的体重是45.0公斤
# 每次跑步会减肥0.5公斤
# 每次吃东西体重会增加1公斤

# 属性：姓名，体重
# 行为：跑步  吃东西
class Person():
    def __init__(self,name,height):
        self.name=name
        self.height=height

    def run(self):
        self.height -= 0.5
        print('{}跑步后，体重{}'.format(self.name,self.height))

    def eat(self):
        self.height += 1
        print('{}吃东西后，体重{}'.format(self.name, self.height))

xiaoming=Person('小明',75)
xiaomei=Person('小美',45)
xiaoming.eat()
xiaomei.run()


# 十：python面向对象基础
# 一、编写一个学生类，有姓名，年龄，性别，英语成绩，数学成绩，语文成绩，方法：求总分，平均分，以及打印学生的信息。
# 类名：Students
# 属性：姓名，年龄，性别，英语成绩，数学成绩，语文成绩   --不固定  构造方法
# 方法：求总分，平均分， 打印学生的信息
class Students():
    def __init__(self,name,age,sex,english,math,chinese):
        self.name=name
        self.age=age
        self.sex=sex
        self.e_score=english
        self.m_score=math
        self.c_score=chinese

    def total_score(self):
        sum = self.e_score+self.m_score+self.c_score
        return sum

    def ave_score(self):
        avg=self.total_score()/3
        return avg

    def info(self):
        print('姓名：{}，总分：{}，平均分：{}'.format(self.name,self.total_score(),self.ave_score()))

zs=Students('张三','18','man',80,90,100)
print(zs.total_score())
print(zs.ave_score())
zs.info()

ls=Students('李四','20','man',70,50,30)
print(ls.total_score())
ls.info()