# 函数封装
def a(yingxiong):
    print(yingxiong["A"])


def jineng_1(yingxiong):
    if yingxiong["类型"] == "法师":
        print("你是法师，注意蓝量")
    print("释放技能1---->" + yingxiong["技能1"])


def jineng_2(yingxiong):
    if yingxiong["类型"] == "法师":
        print("你是法师，注意蓝量")
    print("释放技能2---->" + yingxiong["技能2"])


def jineng_3(yingxiong):
    if yingxiong["类型"] == "法师":
        print("你是法师，注意蓝量")
    print("释放技能3---->" + yingxiong["技能3"])


# 用户操作处理
print("欢迎进入王者联盟 - 新手训练营")
while True:
    print("当前已开放英雄：安琪拉、后羿")
    input_type = input("请输入你要试用的英雄(输入exit退出)：")
    if "exit" == input_type:
        break
    if "后羿" == input_type:
        yingxiong = {
            "英雄名称": "后羿",
            "A": "平A一下",
            "技能1": "强化攻击",
            "技能2": "天降太阳",
            "技能3": "射一只鸟",
            "回城": "回城中...",
            "回血": "回血中...",
            "闪现": "闪现...",
            "移动": "移动...",
            "类型": "射手"
        }
    if "安琪拉" == input_type:
        yingxiong = {
            "英雄名称": "安琪拉",
            "A": "平A一下",
            "技能1": "喷射火焰",
            "技能2": "火球转转",
            "技能3": "喷射火龙",
            "回城": "回城中...",
            "回血": "回血中...",
            "闪现": "闪现...",
            "移动": "移动...",
            "类型": "法师"
        }
    # 变动1： 如果有一百个英雄， 类似的重复代码，写一百次。 以后有改动，需要改一百次

    while True:
        chose = input('请选择你要对进行的操控："A","技能1","技能2","技能3","回城","回血","闪现","移动"(输入exit退出)：')
        if "A" == chose:
            a(yingxiong)
        elif "技能1" == chose:
            jineng_1(yingxiong)
        elif "技能2" == chose:
            jineng_2(yingxiong)
        elif "技能3" == chose:
            jineng_3(yingxiong)
        elif "exit" == chose:
            break
        else:
            print("其他功能没实现...")
