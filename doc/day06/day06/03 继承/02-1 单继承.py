"""单继承



语法：
class 类名(父类)：
	子类属性
	 def 子类特有的方法
	…

子类可以享有父类的所有方法和属性。
子类中如果有同名方法，那么以子类方法优先（父类方法不再执行）
子类重写父类方法
"""
# 案例1：
# 定义一个动物类，定义一个狗类和猫类继承动物类
# 动物类有属性type_name和吃、睡的实例方法
# 狗类叫的实例方法
# 猫类有叫的实例方法
# 实例狗和猫类的对象，分别调用父类的属性和方法

# 父类：动物类 Animal
# 属性：type_name
# 行为：吃、睡
class Animal():
    type_name='动物类'
    __num=100

    def eat(self):
        print('动物吃肉')

    def sleep(self):
        print('动物睡觉少')

class Dog(Animal):
    def shout(self):
        print("汪汪汪")

class Cat(Animal):
    def shout(self):
        print("喵喵喵")

wangcai = Dog()
wangcai.sleep()
wangcai.eat()
wangcai.shout()
print(wangcai.type_name)
mimi = Cat()
mimi.shout()
mimi.eat()