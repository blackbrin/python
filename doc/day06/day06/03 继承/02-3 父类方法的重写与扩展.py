''''''
'''重写父类方法
覆盖父类的方法，重写父类的方法，子类的方法名与父类一致。
'''
# 案例3：
# 定义一个动物类，定义一个猫类，猫类继承动物类
# 动物类有属性type_name和吃、睡的实例方法
# 猫类有属性type_name和吃、叫的实例方法
# 实例猫类的对象，分别调用属性和方法
class Animal():
    type_name='动物类'
    __num=100

    def eat(self):
        print('动物吃肉')

    def sleep(self):
        print('动物睡觉少')
#
# # 子类：
# # 狗类 Dog
# # 行为：叫
class Dog(Animal):
    def shout(self):
        print('汪汪汪')

    def sleep(self):
        Animal.sleep(self) # 扩写 父类的方法
        print("狗一般很懒") # 增加子类方法逻辑

wangcai = Dog()
wangcai.eat()
wangcai.shout()
wangcai.sleep()
# #     # 父类方法的重写---子类方法名和父类方法名一致
# #     # 什么时候用到重写：父类的方法跟子类的功能完全不一致
# #     def eat(self):
# #         print("小狗爱吃骨头")
# #
# # # 猫类 Cat
# # # 行为：叫
# # class Cat(Animal):
# #     def shout(self):
# #         print('喵喵喵')
# #
# #     def eat(self):
# #         print("小猫爱吃鱼")
# #
# # xiaohuang=Dog()
# # xiaohuang.eat()  #骨头
# # xiaohua=Cat()
# # xiaohua.eat()  #鱼
#
#
#
#
# '''扩展父类方法'''
# # 案例4：
# # 定义一个猫类和HelloKitty猫类，其中HelloKitty类继承猫类
# # 猫类有type_name属性和吃、叫的实例方法
# # HelloKitty类有叫的方法
# # 实例HelloKitty类的对象，分别调用方法
#
# class Animal():
#     type_name='动物类'
#     __num=100
#
#     def eat(self):
#         print('动物吃肉')
#
#     def sleep(self):
#         print('动物睡觉少')
#
# # 子类：
# # 狗类 Dog
# # 行为：叫
# class Dog(Animal):
#     def shout(self):
#         print('汪汪汪')
#     # 父类方法的扩写---子类方法名和父类方法名一致
#     # 什么时候用到扩写：父类的方法实现了部分需求，在子类中要增加新的需求
#     def eat(self):
#         # 调用父类方法：父类名.方法(self,参数1...)
#         Animal.eat(self)
#         print("小狗爱吃骨头")
#
# # 猫类 Cat
# # 行为：叫
# class Cat(Animal):
#     def shout(self):
#         print('喵喵喵')
#
#     def eat(self):
#         print("小猫爱吃鱼")
#
# xiaohuang=Dog()
# xiaohuang.eat()  #骨头
# # xiaohua=Cat()
# # xiaohua.eat()  #鱼