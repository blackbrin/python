
''''''
'''重写父类方法
语法：
多继承语法：
class 类名(父类1,父类2)：
	子类属性    
	def 子类特有的方法
	    …
'''
# 案例：
# 定义一个神仙类和猴子类，再定义一个孙悟空类，孙悟空类分别继承神仙类和猴子类
# 神仙类有飞的实例方法
# 猴子类有吃的实例方法
# 孙悟空类有取经的实例方法
# 实例孙悟空对象，分别调用父类的属性和方法
class Shenxian():
    def fly(self):
        print('神仙会飞')

class Monkey():
    def eat(self):
        print('猴子爱吃香蕉')

class SunWuKong(Monkey,Shenxian):
    def qujing(self):
        print('西天取经')

swk=SunWuKong()
swk.fly()
swk.eat()
swk.qujing()