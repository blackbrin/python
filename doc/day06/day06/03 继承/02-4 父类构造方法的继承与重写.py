
''''''
'''父类构造方法的继承与重写
情况一：父类有构造方法，子类没有，子类实例化对象时，会自动化调用父类的构造方法
'''
# class Father():
#     def __init__(self,name):
#         print('父类构造方法：{}'.format(name))
#
# class Son(Father):
#     def eat(self):
#         print('吃喝玩乐')
#
# s1=Son('son')  #子类实例对象时，会自动调用父类的构造方法
# s1.eat()

'''
情况二：父类有构造方法，子类也有构造方法，子类实例化对象时，会优先使用自己类的构造方法
'''
# class Father():
#     def __init__(self, name):
#         print('父类构造方法：{}'.format(name))
#
# class Son(Father):
#     def __init__(self, name):
#         print('子类构造方法：{}'.format(name))
#
#     def eat(self):
#         print('吃喝玩乐')
#
# s2=Son('ss')  #实例对象时，使用自己的构造方法
# s2.eat()

'''
情况三：父类有构造方法，子类也有构造方法，并且子类的构造方法中调用父类的构造方法并进行扩展
子类调用父类构造方法的语法：
    super(子类名，self).__init__(参数1,参数2,....)
    父类名称.__init__(self,参数1,参数2,...)  --推荐这种方式，容易理解和记忆
'''
class Father():
    def __init__(self, name):
        print('父类构造方法，姓名{}'.format(name))
    def eat(self):
        print("我喜欢吃鱼")

class Son(Father):
    def __init__(self,name,age):
        Father.__init__(self,name)   #推荐这种方式
        # super(Son,self).__init__(name)
        print('年龄：{}'.format(age))

    def eat(self):
        Father.eat(self)
        print('吃喝玩乐')

s3 = Son('ss',18)  # 实例对象时，使用自己的构造方法
s3.eat()
