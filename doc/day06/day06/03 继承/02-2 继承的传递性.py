
''''''
'''继承的传递性'''
# 案例2：
# 定义一个动物类，定义一个猫类和HelloKitty猫类，其中猫类继承动物类，HelloKitty类继承猫类
# 动物类有属性type_name和吃、睡的实例方法
# 猫类有叫的实例方法
# HelloKitty类有说话的方法
# 实例猫类和HelloKitty类的对象，分别调用父类的属性和方法
# 爷爷辈
class Animal():
    type_name='动物类'
    __num=100

    def eat(self):
        print('动物吃肉')

    def sleep(self):
        print('动物睡觉少')

# 爸爸辈
class Cat(Animal):
    def shout(self):
        print('喵喵喵')

# 孙子辈
class HelloKitty(Cat):
    def speak(self):
        print('我是hello kitty')

hk=HelloKitty()
hk.speak()
hk.shout()
hk.eat()