class Yingxiong():

    # name = "安琪拉"  # 类级别的变量 --
    def __init__(self, name, desc_1, desc_2, desc_3):  # 构造 --
        """
        :param name:
        :param desc_1:
        :param desc_2:
        :param desc_3:
        """
        self.name = name
        self.desc_1 = desc_1
        self.desc_2 = desc_2
        self.desc_3 = desc_3

    def a(self):
        """
        所有英雄对象 都具备的一个方法，查看 a 的技能描述
        :return:
        """
        print("平A一下")

    def jineng_1(self):
        print("释放技能1---->" + self.desc_1)

    def jineng_2(self):
        print("释放技能2---->" + self.desc_2)

    def jineng_3(self):
        print("释放技能3---->" + self.desc_3)

    def huicheng(self):
        print("回城中...")

    def huixue(self):
        print("回血中...")

    def shanxian(self):
        print("闪现...")

    def yidong(self):
        print("移动...")

class Fashi(Yingxiong):
    def jineng_1(self):
        print("法师，请注意蓝量消耗")
        print("释放技能1---->" + self.desc_1)

    def jineng_2(self):
        print("法师，请注意蓝量消耗")
        print("释放技能2---->" + self.desc_2)

    def jineng_3(self):
        print("法师，请注意蓝量消耗")
        print("释放技能3---->" + self.desc_3)

yx = Yingxiong("安琪拉","火焰喷射","火球转转","火焰图西")
# 函数封装
def a(yingxiong):
    print(yingxiong["A"])

def jineng_1(yingxiong):

    print("释放技能1---->" + yingxiong["技能1"])

def jineng_2(yingxiong):
    if yingxiong["类型"] == "法师":
        print("你是法师，注意蓝量")
    print("释放技能2---->" + yingxiong["技能2"])

def jineng_3(yingxiong):
    if yingxiong["类型"] == "法师":
        print("你是法师，注意蓝量")
    print("释放技能3---->" + yingxiong["技能3"])


# 用户操作处理
print("欢迎进入王者联盟 - 新手训练营")
while True:
    print("当前已开放英雄：安琪拉、后羿")
    # 针对法师类型 加提示
    input_type = input("请输入你要试用的英雄(输入exit退出)：")
    if "exit" == input_type:
        break
    if "后羿" == input_type:
        yingxiong = Yingxiong("后羿","强化攻击","天降太阳","射一只鸟")
    if "安琪拉" == input_type:
        yingxiong = Fashi("安琪拉", "喷射火焰", "火球转转", "喷射火龙")
    # 变动1： 如果有一百个英雄， 类似的重复代码，写一百次。 以后有改动，需要改一百次

    while True:
        chose = input('请选择你要对进行的操控："A","技能1","技能2","技能3","回城","回血","闪现","移动"(输入exit退出)：')
        if "A" == chose:
            yingxiong.a()
        elif "技能1" == chose:
            # jineng_1(yingxiong)
            yingxiong.jineng_1()
        elif "技能2" == chose:
            yingxiong.jineng_2()
        elif "技能3" == chose:
            yingxiong.jineng_3()
        elif "exit" == chose:
            break
        else:
            print("其他功能没实现...")
