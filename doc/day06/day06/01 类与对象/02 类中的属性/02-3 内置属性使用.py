
''''''
'''内置属性
类名.__dict__ : 类的属性（字典，由类的数据属性组成）
类名.__doc__ :类的文档字符串
类名.__name__: 类名
类名.__module__: 类定义所在的模块（类的全名是'__main__.className'，如果类位于一个导入模块mymod中，那么className.__module__ 等于 mymod）
类名.__bases__ : 类的所有父类构成元素（包含了一个由所有父类组成的元组）
'''
class Person():
    typ=0
    # 构造方法
    def __init__(self,sname,sage,ssex,sjob):  #self:对象
        self.name=sname
        self.age=sage
        self.__sex=ssex
        self.__job=sjob

    def eat(self):
        print('{}爱吃'.format(self.name))

    def work(self):
        print('工作:{}'.format(self.__job))

class Student():
    pass

class Teacher():
    pass

print(Person.__dict__ )