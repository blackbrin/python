
''''''
'''实例属性
  
实例属性：实例对象的属性特征
    定义在构造方法中的属性
    公有实例属性:不已两个下划线开头的；都可以使用--类里面，类外面也可以用，子类也可以用
    私有实例型性:已两个下划线开头的；只能在自己类里面可以用，出了类就不能用了
    
    调用的语法：
        在类的外面调用：对象名.属性名
        在类的里面调用：self.属性名   self==对象
'''
class Person():
    # 构造方法
    def __init__(self,sname,sage,ssex,sjob):  #self:对象
        self.name=sname
        self.age=sage
        self.__sex=ssex
        self.__job=sjob

    def eat(self):
        print('{}爱吃'.format(self.name))

    def work(self):
        print('工作:{}'.format(self.__job))

shanchen=Person('小明',24,'man','student')
sanfeng=Person('sanfeng',24,'female','teacher')
print(shanchen.name)  #'小明'   # 字典 是 dict[]  自定义类型对象 .号
print(shanchen.__sex)  #AttributeError: 'Person' object has no attribute '__sex'

shanchen.eat()
sanfeng.work()