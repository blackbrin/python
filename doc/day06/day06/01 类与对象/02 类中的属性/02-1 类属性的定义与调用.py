
''''''
'''类中的属性
类属性：整个类的属性特征
    定义在类中，不是在函数中的属性
    公有类属性:不已两个下划线开头的；都可以使用--类里面，类外面也可以用，子类也可以用
    私有类型性:已两个下划线开头的； 只能在自己类里面可以用，出了类就不能用了
    
    调用格式：类名.属性名
'''
class Person():
    # 类属性
    type='人类'
    num=0
    __phone=13011112222

    def eat(self):
        print(Person.type) #人类
        print('爱吃是天性')

    def work(self):
        print(Person.__phone)  #13011112222
        print('不工作就没钱')

# 类属性的使用
print(Person.type)

p1=Person()
p1.eat()

# print(Person.__phone)  #AttributeError: type object 'Person' has no attribute '__phone'
p1.work()