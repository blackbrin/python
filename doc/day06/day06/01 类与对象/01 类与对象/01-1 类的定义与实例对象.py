
''''''
'''类的定义

class 类名():
	属性（特征)
	函数（功能）

'''

class yingxiong():
    name = "任意英雄"
    desc_1 ="技能1"
    desc_2 = "技能2"
    desc_3 = "技能3"
    type = "英雄类型"

    # 构造参数
    def __init__(self,name,desc_1,desc_2,desc_3,type):
        self.name = name
        self.desc_1 = desc_1
        self.desc_2 = desc_2
        self.desc_3 = desc_3
        self.type = type

    def a(self):
        print("平A一下")

    def jineng_1(self):
        print("释放技能1--->"+self.desc_1)

    def jineng_2(self):
        print("释放技能2---->" + self.desc_2)

    def jineng_3(self):
        print("释放技能3---->" + self.desc_3)

    def huicheng(self):
        print("回城中...")

    def huixue(self):
        print("回血中...")

    def shanxian(self):
        print("闪现...")

    def yidong(self):
        print("移动...")





# 案例1：创建一个类：
# 类名字：Person
# 属性：姓名  年龄  性别   职业
# 功能：吃  工作
# 例如: 我们要在系统里面 存放关于 人类数据的时候， 我们不应用 字典
# 我们应该自定义类型 User  --
class Person():
    name='晱辰' # 类级别的 变量定义 【多个实例对象之间，共享】
    age=18
    sex='man'
    job='student'

    def eat(self):
        print('爱吃是天性')

    def work(self):
        print('不工作就没钱')
#
# '''类的实例化
# 对象名=类名()
# '''
# 案例2：晱辰    --对象
shanchen=Person()
# 	三丰老师  --对象
sanfeng=Person()
print(shanchen)
print(sanfeng)
#
#
# '''对象使用类中属性'''
# # 案例3：shanchen的属性信息
print(shanchen.name)  #晱辰
print(sanfeng.name)  #晱辰  -->如何去优化？如何做到每个对象都具有各自的特征？构造方法
#
# # 修改属性
# shanchen.age=24
# print(shanchen.age)  #24
# print(sanfeng.age)   #18
#
#
# '''对象使用类中方法'''
# # 案例5：shanchen
# shanchen.eat()  #肉     --》优化空间?每个对象功能实现的特征有些不同
# sanfeng.eat()