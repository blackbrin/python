
''''''
'''构造方法

'''
# 如何去优化？如何做到每个对象都具有各自的特征？构造方法  --方法/函数
class Person():
    name='晱辰'
    age=18
    sex='man'
    job='student'
    # 构造方法
    def __init__(self,sname,sage,ssex,sjob):  #self:当前这个对象
        self.name=sname
        self.age=sage
        self.sex=ssex
        self.job=sjob

    def eat(self):
        print('爱吃是天性')

    def work(self):
        print('不工作就没钱')

# 实例对象:实例对象时会自动调用构造方法；在使用构造方法时，会自动将对象本身传给self
shanchen=Person('小明',24,'man','student')
sanfeng=Person('sanfeng',24,'female','teacher')
print(shanchen)
print(sanfeng)
# 对象使用属性
print(shanchen.eat())
print(sanfeng.name)