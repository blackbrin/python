
''''''
'''实例方法
实例方法：第一个参数默认是self,  self 表示这个对象本身
类别：
    公有实例方法：方法名不以两个下划线开头的；都可以访问，类里面，类外面，子类中都可以用
    私有实例方法：方法名以两个下划线开头的；只能在类里面用，出了类就不能用了
    
    调用的语法：
        在类里面调用：self.方法名()
        在类外面调用：对象名.方法名()
'''
class Person():
    def eat(self,*args):
        sum=0
        for i in args:
            sum += i
        print(sum)
        print('{}爱吃是天性'.format(args[0]))

    def __work(self):
        print('不工作就没钱')

    def info(self):
        # self.eat()
        self.__work()
        print('这是info信息')

p1=Person()
p1.eat(10,20,30)    #爱吃是天性
# p1.__work()  #AttributeError: 'Person' object has no attribute '__work'
p1.info()  #p1-self   self.__work()-->p1.__work()

'''重点：
1：两种思想的区分
2：类和对象，定义一个类和实例化对象
3：属性--调用类属性和实例属性
4：方法--定义实例方法，以及调用实例方法
'''