""""""
"""多态
为不同的数据类型提供统一的接口 
计算机程序 运行的时候，相同的内容会送给不同类别的对象。
系统根据你对象的区别，引发不同的行为
"""
# 案例：
# 微信支付 支付宝支付 银行卡支付
#支付接口，不管你是个什么类型都能支付

class Payment():
    def pay(self):
        print("采用线下付款")

class Wechat(Payment):
    def pay(self):
        print("微信支付")
        print("打开微信，展示付款码")

class Ali(Payment):
    def pay(self):
        print("支付宝支付")
        print("打开支付宝，展示付款码")

class Card(Payment):
    def pay(self):
        print("银行卡支付")
        print("打开银行卡，刷卡")


class StartPay():
    def pay(self,obj): # 要操作的对象丢进来
        obj.pay()

p = Card()
s = StartPay()
s.pay(p)