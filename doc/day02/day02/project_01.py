# 烤鸭店利润计算器【】
# 0. 开发语言介绍，环境安装，语法
# 需求 ： 输入 鸭子成本价 ， 鸭子销售价， 鸭子的数量
# 程序输出 利润
print("烤鸭店利润计算器开始工作")

price_1 = input("请输入鸭子成本价:") # 用户输入的内容 -- 变量保存
price_2 = input("请输出鸭子的销售价:") # 用户输入的内容
num = input("请输入鸭子的数量:")

num_price_1 = int(price_1)
num_price_2 = int(price_2)
num_num = int(num)

result = (num_price_2 - num_price_1) * num_num

print("你今天盈利额是:",result)