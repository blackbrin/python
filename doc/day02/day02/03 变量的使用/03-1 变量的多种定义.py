#  写一段公式
# 变量的应用
num_1 = 99987
num_2 = 87634

result = num_1+num_2
print(result)
# 直接解答 课后解答 后面自动会懂 [记录问题]

# 变量的赋值方式 -- 变量的内存地址
# id() -->查看内存地址位置
# 方式一: 单个变量的赋值
age1 = 10
age2 = 10
print(age1, id(age1))
print(age2, id(age2))

# 方式二：连续赋值
age1 = age2 = 10
print(age1)
print(age2)

# 方式三: 多个变量，不同赋值
name, age, sex = "xiaoming", 18, 'female'
print(name)
print(age)
print(sex)

# 方式四 :*号的使用
# * ：将多个值给到一个变量
*a, b, c = 1, 2, 3, 4, 5, 6
print(a)
print(b)
print(c)


