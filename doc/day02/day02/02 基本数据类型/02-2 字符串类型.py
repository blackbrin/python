""" 字符串 str
 定义: 只要是引号引起来的内容，它就是字符串
 比如：单引号，比如双引号 三引号
"""

name = "xiaoming"
sex  = 'female'
addr = '''长沙'''
print(type(name))
print(type(sex))
print(type(addr))

# 任何的数字类型，都可以直接转换成字符串，只需要在上面加上引号就行了

# 几乎被引号引起来的所有内容，它都会理解为字符串，只有某种情况例外
sex  = 'fe"m"ale'
print(sex)

sex  = "fe'm'ale"
print(sex)

#  转义字符 \  -- 转义字符里 -- \n 代表换行  \t 表示空格
#  --不是每个字母都有含义
# 处理转义字符的方式:
# 方式一: 在转义字符前多加一个\  E:\\python基础\\day01
# 方式二： 整个字符前 直接加一个R/r read
str1 = r"hello pytho\n 你好 "
print(str1)

str2 = '\\\\'
print(str2)

sex  = "fe\"m\"ale" # 字符串的\ 可以直接理解为 原样输出
print(sex)


