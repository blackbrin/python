"""
type() 查看变量的数据类型
数字类型:
   整数 : int 由数字0-9组成
   浮点数: float 数字+小数点构成
   布尔值: bool False True
   复数: 实数+虚数
"""
#整数  number --数字类型
price = 9
print(type(price))

#  浮点数 -- 小数
v_price = 99.9
print(type(v_price))

# 布尔值  对 错 True False
res = 1 < 2
print(res)
print(type(res))

#复数
com = 100 + 0.009j
print(com)
print(type(com))