#  给烤鸭店老板做一个利润计算器
#  我们现在应该干什么？ -- 怎么去计算出利润
#  售价 - 成本价 * 卖出去的鸭子数量
print("烤鸭店利润计算器开始工作")

# 让老板自己输入 -- 菜市场 30 大市场 10  养殖基地 15
price_1 = input("请输入你的成本价:")  # 老板自己输入的成本价-- 变量中去
price_2 = input("请输入你的售卖价:")  # 老板自己输入售卖价格
num = input("请输入你卖出的鸭子数:")     # 老板自己卖了多少鸭子
# 数据类型的转换 -- 变成什么类型  变量前 用 int() float() str() -- 数据类型的转换

num_price_1 = int(price_1)
num_price_2 = int(price_2)
num_num = int(num)


result = (num_price_2 - num_price_1) * num_num  # + - * / python数学运算符
print("今天你的利润额度为{}元钱".format(result))