"""转换函数 将不同的数据类型转为指定类型的数据
 字符串 == 整数 int（）
 字符串 == 浮点数 float()
 字符串 == 布尔值 bool()
 整数 === 字符串 str()
"""

# 前提 ：str 引号中必须是数字
str1 = "100"
str2 = "3.1111"
num = float(str2)
print(num)
print(type(num))