import pytest

class TestDemo(object):
    data = [(1,2,3),
            (2,2,4)]

    @pytest.mark.parametrize("a,b,c",data)
    def test_add(self, a, b, c):
        result = a + b
        assert c == result

    def test_str(self):
        str = "felix"
        assert "f" in str

if __name__ == '__main__':
    pytest.main(["-sv","test_01.py::TestDemo::test_add"])