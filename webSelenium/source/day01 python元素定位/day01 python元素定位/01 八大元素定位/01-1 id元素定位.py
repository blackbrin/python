""""""
"""
import -- 简单的包 -math 类偏少 有的甚至只有一个类
from -- 复杂的包 -- 多个类 --每个类都有自己的方法 
webdriver 提供一个打开浏览器的方法 -- 
"""
from selenium import webdriver
import time
""" 这就是完整的webUI 搜索功能测试流程
 让python打开浏览器需要驱动
"""
# 0 加载驱动
driver_path = r"D:\driver\95ver\chromedriver.exe"
# 1.打开浏览器
driver = webdriver.Chrome(executable_path=driver_path)
# 2.输入网址
driver.get("http://novel.hctestedu.com/")
# 3.找到元素
search_input = driver.find_element_by_id("searchKey")
print(search_input.tag_name)
print(search_input.get_attribute("type"))
print(search_input.parent)
# 4.输入关键字
search_input.send_keys("江少")
time.sleep(1)
# 5.点击搜索
driver.find_element_by_id("btnSearch").click()
time.sleep(1)
# 6.验证结果 --assert 断言
""" 断言 判断一个表达式（True/False）
返回是False --触发异常 
特性：条件不满足直接返回错误，不必再继续运行了
"""
assert driver.title == "全部作品_读书屋"
print("测试通过")